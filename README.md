# OpenML dataset: Video-Game-Use-in-US-High-School-Students

https://www.openml.org/d/43408

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This is a subset from the Monitoring the Future Survey of high school students. My focus in working on this is trying to understand the effect that technology use has on grades (GPA).
Content
The columns are labeled. Technology use (PC, videogame use, etc.) as well as Grades (GPA) are labeled accordingly.
Acknowledgements
This dataset is the result of Monitoring the Future, at the University of Michigan.
Inspiration
Is GPA affected by the use of technology?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43408) of an [OpenML dataset](https://www.openml.org/d/43408). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43408/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43408/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43408/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

